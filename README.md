# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### SETUP ###
- Installe python on your computer
- Get AWS IAM SECRET ( ASK ST )
- Configure aws cli on your computer ( type aws configure and set the parameters)
- Install boto3 for python
Docs : https://boto3.amazonaws.com/v1/documentation/api/latest/guide/quickstart.html


### How to use ?  ###
Open your terminal go in the root path of this repository
Modify each parameter "YOURBUCKET" "YOURFOLDER" of the following command
Execute this command : 
python .\downloader.py --bucket "YOURBUCKET" --folder "YOURFOLDER"



### TO DO ###
- Allow user to select folder from a bucket
- Add all download file to .gitignore